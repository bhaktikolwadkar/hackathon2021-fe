import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTickerListComponent } from './stock-ticker-list.component';

describe('StockTickerListComponent', () => {
  let component: StockTickerListComponent;
  let fixture: ComponentFixture<StockTickerListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StockTickerListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StockTickerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
