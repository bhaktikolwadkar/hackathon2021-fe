import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../shared/rest-api.service';
import { BoughtStocksService } from "../shared/bought-stocks.service";
import { GetPriceService } from '../shared/get-price.service';
import { HoldingsListService } from '../shared/holdings-list.service';

@Component({
  selector: 'app-sell-order',
  templateUrl: './sell-order.component.html',
  styleUrls: ['./sell-order.component.css']
})
export class SellOrderComponent implements OnInit {
  @Input() stockDetails = { id: 0, stockTicker: '', price: 0.00, date:"", volume: 0, buyOrSell: "Sell", statusCode: 0 }
  @Input() holdingsDetails = {  stockTicker: '', volume: 0,buying_price:0,current_price:0 }

  value: number = 0;
  currentValue: any = [];
  Data2: any = [];
  holdings: any = [];
  temp: any;
  Temp: boolean = false;
  Stocks: any=[];
  date: any;

  constructor(
    public RestApi: RestApiService,
    public router: Router,
    public boughtStocks: BoughtStocksService,
    public getPrice: GetPriceService,
    public holdingsList: HoldingsListService
  ) { }

  ngOnInit(): void {
    this.loadData()
  }

  loadPrice() {
    return this.getPrice.getPrice(this.stockDetails.stockTicker).subscribe((data: {}) => {
      this.currentValue = data;
      this.value = this.currentValue["price_data"][0][1];
      this.stockDetails.price = this.value;
      this.date = this.currentValue["price_data"][0][0];
      this.stockDetails.date = this.currentValue["price_data"][0][0];
    })
  }

  sellStock() {
    this.RestApi.createStock(this.stockDetails).subscribe((data: {}) => {
      this.router.navigate(['/trade-history'])

      setTimeout(() => {return this.removeHoldings()}, 25000);
    })
  }

  loadData() {
    return this.boughtStocks.getData().subscribe((data: {}) => {

      this.Data2 = data;



      console.log({ data })
    })
  }
  onOptionsSelected(value: string) {
    this.loadPrice();
    console.log("the selected value is " + this.stockDetails.stockTicker);
  }



  confirmAction() {
    let confirmAction = confirm("Are you sure to execute this action?");
    if (confirmAction) {
      this.sellStock();
     
      alert("Sold Stocks of " + this.stockDetails.stockTicker + "  of Quantity  " + this.stockDetails.volume );
    } else {
      alert("Trade Rejected");
    }
  }
  removeHoldings() {

    this.RestApi.getStocks().subscribe((data: {}) => {
      this.Stocks = data;
      
     
    
    console.log(this.Stocks)
    var temp = Number(this.Stocks[this.Stocks.length - 1].statusCode);

    console.log(temp);
    if(temp == 2){

    this.holdingsList.getHoldings().subscribe((data: {}) => {

      this.holdings = data;
      
      this.holdingsDetails.stockTicker = this.stockDetails.stockTicker;
      
      
      for (var i = 0; i < this.holdings.length; i++) {
        console.log("in for");
        if (this.holdingsDetails.stockTicker === this.holdings[i].stockTicker) {
          console.log("in if");
          this.holdingsDetails.volume = Number(this.holdings[i].volume) - Number(this.stockDetails.volume);
          this.holdingsDetails.buying_price = this.holdings[i].buying_price;
          if (this.holdingsDetails.volume >= 0) {
            console.log("in 2nd if");
            this.Temp = true;
            break;
          }

        }
      }
      if (this.Temp == true) {
        if (this.holdingsDetails.volume > 0) {
          this.holdingsList.deleteHoldings(this.holdingsDetails.stockTicker).subscribe((data: {}) => {
            return this.holdingsList.createHoldings(this.holdingsDetails).subscribe((data: {}) => {
            })
          })
        }

        else {
          return this.holdingsList.deleteHoldings(this.holdingsDetails.stockTicker).subscribe((data: {}) => {
          })
        }
      }
      else{
      return this.RestApi.deleteStock(this.Stocks[this.Stocks.length-1].id).subscribe((data:{}) => {alert("You do not have enough stocks to sell");});
      }
      return
    })
  }
 } )}

}
