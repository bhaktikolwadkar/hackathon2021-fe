import { Component, OnInit, ViewChild, ElementRef, Input } from '@angular/core';
import * as d3 from "d3";
import { RestApiService } from "../shared/rest-api.service";
import { DataAccessService } from "../shared/data-access.service"
import { GetPriceService } from '../shared/get-price.service';
import { Router } from '@angular/router';
import { NumberValue } from 'd3';
//import * as CanvasJS from 'canvasjs';
//import CanvasJS from 'canvasjs';
@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss']
})
export class AnalysisComponent implements OnInit {
  @Input() stockDetails1 = { stockTicker: '' }
  currentValue: any=[];
  Data1: any=[];
  days: any=[];
  daysData: any=[];
  price: Array<NumberValue>=[];
  value: any=[];
  stat: number=0;
  
  private width = 200;
  private height = 200;
  private margin = 50;
  public svg;
  public svgInner;
  public yScale;
  public xScale;
  public xAxis;
  public yAxis;
  public lineGroup;

  constructor(
    public RestApi: RestApiService,
    public router: Router,
    public dataAccess: DataAccessService,
    public getPrice: GetPriceService,
    public chartElem: ElementRef,
  ) { }

  ngOnInit(): void {
    this.loadData();
    
  }
  loadData(){
    return this.dataAccess.getData().subscribe((data: {}) => {
      this.Data1 = data;
  })
  }

  loadPrice(){
    return this.getPrice.priceHistory(this.stockDetails1.stockTicker, this.days).subscribe((data: {})=>{
        this.currentValue =  data;
        this.value = this.currentValue["price_data"]
        const total = this.value.length;
        let x:String[]=[]
        for (let i=0; i<total; i++){
          this.price[i] = this.value[i][1]
          this.daysData[i] = this.value[i][0]
          //x[i]=Object.values(this.daysData[i]).toString()
          //console.log(typeof(x[i]))
          //this.daysData[i].toString()
          //console.log(this.daysData)
          //console.log(Object.keys(this.daysData))
          
        }
        this.getUpOrDown(this.price); 
    })

  }

  onOptionsSelected(value:string){
    this.loadPrice();
    console.log("the selected value is " + this.stockDetails1.stockTicker);
    
    
}
getUpOrDown(price){
 for (var i=0;i<this.price.length;i++){
 
  var x = this.price[i-1];
      if(i==0){
        this.stat = 3;
      }
      else if (this.price[i] > x){
         this.stat = 0;
      }
      else if (this.price[i] < x){
        this.stat = 1;
      }
      else if (this.price[i] == x){
        this.stat=2;
      }
      else{
        this.stat=3;
      }
      console.log("Stat ",this.stat)
    }

//return this.stat;
}

createTable(){
  
}
}
/*create(){
  let chart = new CanvasJS.Chart("chartContainer", {
		animationEnabled: true,
		exportEnabled: true,
		title: {
			text: "Basic Column Chart in Angular"
		},
		data: [{
			type: "column",
			dataPoints: [
				{ y: 71, label: "Apple" },
				{ y: 55, label: "Mango" },
				{ y: 50, label: "Orange" },
				{ y: 65, label: "Banana" },
				{ y: 95, label: "Pineapple" },
				{ y: 68, label: "Pears" },
				{ y: 28, label: "Grapes" },
				{ y: 34, label: "Lychee" },
				{ y: 14, label: "Jackfruit" }
			]
		}]
	});
		
	chart.render();
    }

}
  initChart(){
    this.svg = d3
    .select(this.chartElem.nativeElement)
    .select('.linechart')
    .append('svg')
    .attr('height', this.height);

    this.svgInner = this.svg
    .append('g')
    .style('transform', 'translate(' + this.margin + 'px, ' + this.margin + 'px)');
    
    const max1=Number(d3.max(this.price))
    const min1=Number(d3.min(this.price))

    this.yScale = d3
    .scaleLinear()
    .domain([max1+1, min1-1])
    //.domain([max1, min1])
    .range([0, this.height-2 * this.margin]);
    console.log("Y", this.yScale)
    
    this.xScale = d3
    .scaleOrdinal(this.daysData)
    .domain((this.daysData))
    console.log("x", this.xScale)
    
    this.yAxis = this.svgInner
    .append('g')
    .attr('id', 'y-axis')
    .style('transform', 'translate(' + this.margin + 'px,  0)');

    this.xAxis = this.svgInner
      .append('g')
      .attr('id', 'x-axis')
      .style('transform', 'translate(0, ' + (this.height - 2 * this.margin) + 'px)');
      
    this.lineGroup = this.svgInner
      .append('g')
      .append('path')
      .attr('id', 'line')
      .style('fill', 'none')
      .style('stroke', 'red')
      .style('stroke-width', '2px')

    this.drawChart();
  }
  
  drawChart(){
    this.width = this.chartElem.nativeElement.getBoundingClientRect().width;
    this.svg.attr('width', this.width);

    this.xScale.range([this.margin, this.width - 2 * this.margin]);

    const xAxis = d3
    .axisBottom(this.xScale)
    .ticks(10)
    //.tickFormat(d3.timeFormat('%m / %Y'));

  this.xAxis.call(xAxis);
  const yAxis = d3
  .axisLeft(this.yScale);

this.yAxis.call(yAxis);

const line = d3
  .line()
  .x(this.daysData)
  .y(Number(this.price))
  .curve(d3.curveMonotoneX);

  const points: [number, number][] = [this.xScale(this.daysData),this.yScale(this.price)];

  this.lineGroup.attr('d', line(points));

  }

}
*/
