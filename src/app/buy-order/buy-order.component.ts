import { Component,ElementRef,Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";
import { DataAccessService } from "../shared/data-access.service"
import { GetPriceService } from '../shared/get-price.service';
import { HoldingsListService } from '../shared/holdings-list.service';

@Component({
  selector: 'app-buy-order',
  templateUrl: './buy-order.component.html',
  styleUrls: ['./buy-order.component.css']
})
export class BuyOrderComponent implements OnInit {
  @Input() stockDetails = { id: 0, stockTicker: '', price: 0.00, volume:0, date:"", buyOrSell:"Buy", statusCode: 0 }
  @Input() holdingsDetails = {stockTicker: '', volume: 0,buying_price:0,current_price:0}

  currentValue: any=[];
  Data1: any=[];
  value: number=0;
  holdings:any=[];
  temp:any;
  Temp :boolean = false;
  Stocks: any=[];
  date: any;


  constructor(
    public RestApi: RestApiService,
    public router: Router,
    public dataAccess: DataAccessService,
    public getPrice: GetPriceService,
    public holdingsList: HoldingsListService
    
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  loadPrice(){
    return this.getPrice.getPrice(this.stockDetails.stockTicker).subscribe((data: {})=>{
        this.currentValue =  data;
        this.value = this.currentValue["price_data"][0][1];
        this.stockDetails.price=this.value;
        this.date = this.currentValue["price_data"][0][0];
        this.stockDetails.date = this.date;
    })
  }

  addStock(){
    this.RestApi.createStock(this.stockDetails).subscribe((data: {}) =>{
     
      this.router.navigate(['/trade-history'])

      setTimeout(() => {return this.insertHoldings()}, 25000);
      
      
    })
  }

  



  loadData(){
   
    return this.dataAccess.getData().subscribe((data: {}) => {
      
      this.Data1 = data;
      
      
  })
  }

  //Pop Up
  confirmAction() {
    let confirmAction = confirm("Are you sure to execute this action?");
    if (confirmAction) {
      this.addStock();
      alert("Bought Stocks of " + this.stockDetails.stockTicker + "  of Quantity  " + this.stockDetails.volume );
    } else {
      alert("Trade Rejected");
    }
  }




////
  insertHoldings(){
    this.RestApi.getStocks().subscribe((data: {}) => {
      this.Stocks = data;
     
    
    console.log(this.Stocks)
    var temp = Number(this.Stocks[this.Stocks.length - 1].statusCode);

    console.log(temp);
    if(temp == 2){
    console.log("in stockdetails func");
    this.holdingsList.getHoldings().subscribe((data: {}) => {
      
      this.holdings = data;
      
      this.holdingsDetails.stockTicker=this.stockDetails.stockTicker;
      this.holdingsDetails.volume=Number(this.stockDetails.volume);
      this.holdingsDetails.buying_price=Number(this.stockDetails.price);
      
      for (var i=0  ; i<this.holdings.length; i++){
        
        if (this.holdingsDetails.stockTicker == this.holdings[i].stockTicker)
        { 
          this.holdingsDetails.volume=Number(this.holdings[i].volume)+Number(this.stockDetails.volume);
          this.holdingsDetails.buying_price=(Number(this.holdings[i].buying_price)*Number(this.holdings[i].volume)+Number(this.stockDetails.volume)*Number(this.stockDetails.price))/this.holdingsDetails.volume;
          this.Temp = true;
          break;
        }
      }
      if(this.Temp ){
         return this.holdingsList.deleteHoldings(this.holdingsDetails.stockTicker).subscribe((data: {})=>{
        
            return this.holdingsList.createHoldings(this.holdingsDetails).subscribe((data: {})=>{
         
        })
      })
    }
    
      return this.holdingsList.createHoldings(this.holdingsDetails).subscribe((data: {})=>{
       
      })
      
  })

}
})
  }
  onOptionsSelected(value:string){
    this.loadPrice();
    console.log("the selected value is " + this.stockDetails.stockTicker);
}

}