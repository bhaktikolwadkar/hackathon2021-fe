import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TradeHistoryComponent } from './trade-history/trade-history.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';

import { BuyOrderComponent } from './buy-order/buy-order.component';
import { StockTickerListComponent } from './stock-ticker-list/stock-ticker-list.component';
import { SellOrderComponent } from './sell-order/sell-order.component';
import { StatusReportComponent } from './status-report/status-report.component';
import { AnalysisComponent } from './analysis/analysis.component';
import { HoldingsListComponent } from './holdings-list/holdings-list.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { IgxTreemapModule } from "igniteui-angular-charts";
import { IgxButtonModule } from 'igniteui-angular';
import { TreeMapModule, TreeMapLegendService, TreeMapTooltipService, TreeMapAllModule } from '@syncfusion/ej2-angular-treemap';




@NgModule({
  declarations: [
    AppComponent,
    TradeHistoryComponent,
    BuyOrderComponent,
    StockTickerListComponent,
    SellOrderComponent,
    StatusReportComponent,
    AnalysisComponent,
    HoldingsListComponent,
    LoginPageComponent,
    AboutUsComponent,
    ContactUsComponent
  

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule, 
    MatSidenavModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    ReactiveFormsModule,
    IgxTreemapModule,
    IgxButtonModule,
    TreeMapModule, 
    TreeMapAllModule
  ],
 
  providers: [TreeMapLegendService, TreeMapTooltipService],
  bootstrap: [AppComponent],
  
})
export class AppModule { }
