import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";

@Component({
  selector: 'app-trade-history',
  templateUrl: './trade-history.component.html',
  styleUrls: ['./trade-history.component.css']
})
export class TradeHistoryComponent implements OnInit {
  combinedArray: {id: any,stockTicker:any, date:any,price:any,volume : any,investment:any,buyOrSell:any,statusCode:any }[] = [];
  Stocks: any = [];
  constructor( 
    public restApi: RestApiService
    ) { }

  ngOnInit(): void {
    this.loadStocks()
  }

  

  loadStocks(){
    return this.restApi.getStocks().subscribe((data: {}) => {
      this.Stocks = data;
      this.Stocks.forEach((fb, index) => this.combinedArray.push({ id: this.Stocks[index].id,stockTicker:this.Stocks[index].stockTicker, date:this.Stocks[index].date,
        price:this.Stocks[index].price,volume : this.Stocks[index].volume,investment:this.Stocks[index].price*this.Stocks[index].volume,buyOrSell:this.Stocks[index].buyOrSell,statusCode:this.Stocks[index].statusCode}));

    })
  }

  deleteStock(id:any){
    if (window.confirm("Are you sure you want to delete the Stock entry?")){
      this.restApi.deleteStock(id).subscribe(data =>{
        this.loadStocks()
      })
    }
  }

}
