import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AnyARecord } from 'dns';
import { GetPriceService } from '../shared/get-price.service';
import { HoldingsListService } from '../shared/holdings-list.service';

@Component({
  selector: 'app-holdings-list',
  styleUrls: ['./holdings-list.component.css'],
  
templateUrl: './holdings-list.component.html',


})
export class HoldingsListComponent implements OnInit {
  combinedArray: {tickerName: any,volume:any, buying_price:any,investment:any,current_price : any,pandl:any }[] = [];
  treemapValues:{Title: any,StockTicker:any, Investment:number}[]=[];
  holdings: any=[];
  currentValue:any;
  value:any;
  price:any;
  holdingsSP:any=[];
  current_price:any=[];
  pandl:any=[];
  totalinvestment:number=0;
  treemapData:object=[];
  data2:object=[];
  stat:number=0;
  constructor(
    public holdingsList: HoldingsListService,
    public getPrice: GetPriceService,
    
    ) { }

  ngOnInit(): void {
    this.loadHoldings();
   
  }
  
  getPandL(){
    for (var i=0;i<this.holdings.length;i++){

        this.pandl.push(this.current_price[i]-this.holdings[i].buying_price);
        

    }
    this.holdings.forEach((fb, index) => this.combinedArray.push({ tickerName:this.holdings[index].stockTicker ,volume:this.holdings[index].volume, buying_price:this.holdings[index].buying_price,investment: this.holdings[index].buying_price*this.holdings[index].volume, current_price: this.current_price[index], pandl: this.pandl[index]  }));
    for (var i=0;i<this.holdings.length;i++){
      this.totalinvestment=this.totalinvestment+this.combinedArray[i].investment;
      
  }
  for (var i=0;i<this.holdings.length;i++){
  if(Number(this.combinedArray[i].current_price-this.combinedArray[i].buying_price) >= 0){
    this.stat = 0;
  }
  else{
    this.stat = 1;
  }
}
  this.initData();
  }

  loadHoldings(){
    return this.holdingsList.getHoldings().subscribe((data: {}) => {
      this.holdings = data;
     // console.log(this.holdings[0]);
      
      for (var i=0;i<this.holdings.length;i++){
        
          this.getPrice.getPrice(this.holdings[i].stockTicker).subscribe((data: {})=>{
              this.currentValue =  data;
              //console.log("in getPrice");
              this.current_price.unshift( this.currentValue["price_data"][0][1]);
              
          })  
        }
        this.holdingsSP=this.current_price;
        setTimeout(() => {return this.getPandL()}, 4000);
  })
  
  }
  initData(): void {
   
   // for (var i=0; i < this.holdings.length;i++){
     
      this.holdings.forEach((fb, i) =>this.treemapValues.push({Title:'Investments',StockTicker:this.combinedArray[i].tickerName,Investment:Number(this.combinedArray[i].investment)}));
      //console.log(this.combinedArray[i].tickerName);
   // }
    console.log(this.treemapValues);
    this.treemapData = this.treemapValues;
    console.log(this.treemapData);
   
  }

  
  public leafItemSettings: object = {
    labelPath: 'StockTicker'
  };

}


