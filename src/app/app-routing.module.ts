import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BuyOrderComponent } from './buy-order/buy-order.component';
import { SellOrderComponent } from './sell-order/sell-order.component';
import { StockTickerListComponent } from './stock-ticker-list/stock-ticker-list.component';
import { StatusReportComponent } from './status-report/status-report.component';
import { TradeHistoryComponent } from './trade-history/trade-history.component';
import { AnalysisComponent } from './analysis/analysis.component';
import { HoldingsListComponent } from './holdings-list/holdings-list.component';
import { LoginPageComponent } from './login-page/login-page.component';
import {AboutUsComponent} from './about-us/about-us.component'
import {ContactUsComponent} from './contact-us/contact-us.component'

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'login-page' },
  { path: 'login-page', component: LoginPageComponent },
  { path: 'trade-history', component: TradeHistoryComponent },
  { path: 'stock-ticker-list', component: StockTickerListComponent },
  { path: 'buy-order', component: BuyOrderComponent },
  { path: 'sell-order', component: SellOrderComponent},
  { path: 'status-report', component: StatusReportComponent},
  { path: 'analysis', component: AnalysisComponent},
  { path: 'holdings-list', component: HoldingsListComponent},
  { path: 'about-us', component: AboutUsComponent},
  { path: 'contact-us', component: ContactUsComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
