import { TestBed } from '@angular/core/testing';

import { BoughtStocksService } from './bought-stocks.service';

describe('BoughtStocksService', () => {
  let service: BoughtStocksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BoughtStocksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
