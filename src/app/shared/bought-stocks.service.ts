import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Data } from '../shared/data';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { Stock } from './stock';

@Injectable({
  providedIn: 'root'
})
export class BoughtStocksService {
  apiURL = 'http://pentagon-hackathon-pentagon-hackathon.punedevopsa7.conygre.com/api/stocks/buy';
  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  
// HttpClient API get() method 
getData(): Observable<Stock> {
  return this.http.get<Stock>(this.apiURL)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}



// Error handling 
handleError(error:any) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  window.alert(errorMessage);
  return throwError(errorMessage);
}

}

