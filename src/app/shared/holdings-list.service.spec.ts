import { TestBed } from '@angular/core/testing';

import { HoldingsListService } from './holdings-list.service';

describe('HoldingsListService', () => {
  let service: HoldingsListService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HoldingsListService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
