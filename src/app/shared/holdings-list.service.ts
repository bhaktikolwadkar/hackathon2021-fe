import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Holdings } from './holdings';

@Injectable({
  providedIn: 'root'
})
export class HoldingsListService {
  
  apiURL = 'http://pentagon-hackathon-pentagon-hackathon.punedevopsa7.conygre.com/api/stocks/holdings/';
  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }), responseType: 'text' as 'json'
  }  
// HttpClient API get() method 
getHoldings(): Observable<Holdings> {
  return this.http.get<Holdings>(this.apiURL)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

// HttpClient API get() method 
getHolding(stockTicker:string): Observable<Holdings> {
  return this.http.get<Holdings>(this.apiURL + stockTicker)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}  

// HttpClient API post() method 
createHoldings(holdings:Holdings): Observable<Holdings> {
  return this.http.post<Holdings>(this.apiURL + '', JSON.stringify(holdings), this.httpOptions)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
} 

// HttpClient API put() method
updateHoldings(stockTicker:String, holdings:Holdings): Observable<Holdings> {
  return this.http.put<Holdings>(this.apiURL, JSON.stringify(holdings), this.httpOptions)
  .pipe(
    retry(1),
    catchError(this.handleError)
  )
}

// HttpClient API delete() method 
deleteHoldings(stockTicker:String){
  console.log(stockTicker);
  return this.http.delete<Holdings>(this.apiURL + stockTicker, this.httpOptions)
  .pipe(
  retry(1),
  catchError(this.handleError)
  )
  
}

// Error handling 
handleError(error:any) {
  let errorMessage = '';
  if(error.error instanceof ErrorEvent) {
    // Get client-side error
    errorMessage = error.error.message;
  } else {
    // Get server-side error
    errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  }
  //window.alert(errorMessage);
  return throwError(errorMessage);
}
}


