import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Data } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Price } from './price';

@Injectable({
  providedIn: 'root'
})
export class GetPriceService {
  apiURL = 'https://jqjfct0r76.execute-api.us-east-1.amazonaws.com/default/PythonPandas';//?ticker=TSLA&num_days=10';
  

  constructor(private http: HttpClient) { }

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }  

  // HttpClient API get() method 
  getPrice(ticker:string): Observable<Price> {
    let str = ticker;
    str = str.replace("^", "-p");
    return this.http.get<Price>(this.apiURL+'?ticker='+str+'&num_days=1',this.httpOptions) //+'&num_days=100'
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }

  priceHistory(ticker:string, days:number): Observable<Price> {
    let str = ticker;
    str = str.replace("^", "-p");
    return this.http.get<Price>(this.apiURL+'?ticker='+str+'&num_days='+days,this.httpOptions) //
    .pipe(
      retry(1),
      catchError(this.handleError)
    )
  }  

  // Error handling 
  handleError(error:any) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
 }
  
}
