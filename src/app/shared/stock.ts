export class Stock {
    constructor(){
        this.id=0;
        this.stockTicker="";
        this.price= 0.00;
        this.date="";
        this.volume = 0;
        this.buyOrSell="";
        this.statusCode = 0;
    }
    id: number;
    stockTicker: string;
    price: number;
    date: string;
    volume: number;
    buyOrSell: string;
    statusCode: number
}
